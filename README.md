## toys [![Build Status](https://travis-ci.org/hryniuk/toys.svg?branch=master)](https://travis-ci.org/hryniuk/toys)

tl;dr This is a sandbox project. Feel free to come in and code something.

#### Idea
This project has longer story than it seems to. In high school, when I was
learning programming using SDL and a few more advanced C++ features, I was
writing short programs based on automata/graphic effects I've read about and
seemed interesting. They have been rotting for years and I not so far ago I
decided to play with them. Now I use this project as my playground to test
ideas, implement new graphic effects and learn new things in C++.

Things I'd like to have
 * console, like in Quake, to choose model, configure it...
 * a few cellular automata and models from **Issues**
 * easy way to add new model (maybe some scripting - thought about Lua(?))
 * shortcuts to control loaded model
 * way to save state/status/screenshot to preserve results (some of models are
   random and because of that unrepeatable)
 * OpenGL integration

#### Building

`toys` depends on `SDL2` and `cppunit` libraries, make sure you've got their
development versions installed.

To build, execute:

```shell
$ mkdir build
$ cd build
$ cmake ..
$ make
```

#### Inspiration / Source of next models
* [NetLogo](https://github.com/NetLogo/NetLogo)
* [Complexification - Jared Tarbell](http://www.complexification.net/gallery/)

#### Contributing
Take a look at [Issues](https://github.com/lukequaint/toys/issues) page (or
TODO file, if you can find any) and do something from the list.

#### License
**GPLv3**

#### Author
Copyright 2010-2016 Łukasz Hryniuk /lukequaint
