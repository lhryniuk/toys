/*
Copyright (C) 2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Color.hpp"

namespace colors
{

const Color black(0, 0, 0);
const Color white(255, 255, 255);
const Color ground_brown(50, 20, 0, 255);
const Color fire(244, 139, 0, 255);
const Color tree_green(0, 255, 0);

const TColorVector black_white =
{
    black,
    white
};

const TColorVector forest_fire =
{
    ground_brown,
    fire,
    tree_green
};

}
