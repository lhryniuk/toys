/*
Copyright (C) 2015 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Point2d.hpp"

Point2d::Point2d(long double x, long double y, state_t state)
    : Atom(EAtomType::Point2d, state), x_{x}, y_{y}
{}


std::vector<long double> Point2d::GetCoordinates() const
{
    return std::vector<long double>{x_, y_};
}
