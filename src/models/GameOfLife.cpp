/*
Copyright (C) 2015 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <iostream>
#include <random>
#include <sstream>

#include "../utils/Cell2d.hpp"
#include "GameOfLife.hpp"

GameOfLife::GameOfLife(int width, int height, bool random_cells, unsigned number_of_states)
    : CellularAutomaton2d(number_of_states, width, height),
      number_of_alive_cells_{0},
      number_of_cells_{width * height}
{
    assert(number_of_states > 1);
    assert(width > 0);
    assert(height > 0);
    if (random_cells) {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<unsigned> sd(0, number_of_states - 1);
        std::uniform_int_distribution<unsigned> hd(0, height - 1);
        std::uniform_int_distribution<unsigned> wd(0, width - 1);
        std::normal_distribution<> d((width * height) / 2, (width * height) / 50);
        int num_of_cells = static_cast<int>(d(gen));
        for (int i = 0; i < num_of_cells; ++i) {
            int x = wd(gen);
            int y = hd(gen);
            states_[y][x] = sd(gen);
            if (states_[y][x]) {
                number_of_alive_cells_++;
            }
        }
    }
}


std::string GameOfLife::Status() const
{
    assert(0 <= number_of_alive_cells_ && number_of_alive_cells_ <= number_of_cells_);
    std::stringstream ss;
    ss << "Number of cells: " << number_of_cells_ << "\n";
    ss << "Number of alive cells: " << number_of_alive_cells_ << "\n";
    ss << "Number of dead cells: " << number_of_cells_ - number_of_alive_cells_ << "\n";
    return ss.str();
}

unsigned GameOfLife::CountCellNeighbours(unsigned x, unsigned y)
{
    assert(x < static_cast<unsigned>(width_) && y < static_cast<unsigned>(height_));
    auto neighbours = 0u;
    for (const auto& n : neighbourhood_full_) {
        int nx = int(x) + n.first;
        int ny = int(y) + n.second;
        auto check = [](int a, int l){return (0 <= a && a < l);};
        if (check(ny, height_) && check(nx, width_) && states_[ny][nx]) {
            ++neighbours;
        }
    }
    return neighbours;
}

state_t GameOfLife::GetNewStateOfCell(unsigned x, unsigned y)
{
    assert(x < static_cast<unsigned>(width_) && y < static_cast<unsigned>(height_));
    state_t new_state{};
    auto number_of_neighbours = CountCellNeighbours(x, y);
    if (states_[y][x]) {
        if (number_of_neighbours == 2 || number_of_neighbours == 3) {
            new_state = 1;
        } else {
            new_state = 0;
        }
    } else if (number_of_neighbours == 3) {
        new_state = 1;
    }
    return new_state;
}

std::vector<std::unique_ptr<Atom>> GameOfLife::Step(int)
{
    std::vector<std::unique_ptr<Atom>> cells;
    for (auto y = 0u; y < unsigned(height_); ++y) {
        for (auto x = 0u; x < unsigned(width_); ++x) {
            auto new_state = GetNewStateOfCell(x, y);
            if (states_[y][x] != new_state) {
                cells.push_back(std::make_unique<Cell2d>(x, y, new_state));
            }
        }
    }
    for (const auto& cell : cells) {
        auto x = cell->GetCoordinates()[0];
        auto y = cell->GetCoordinates()[1];
        states_[y][x] = cell->GetState();
        int delta = 0;
        if (states_[y][x]) {
            delta++;
        } else {
            delta--;
        }
        number_of_alive_cells_ += delta;
    }
    return cells;
}
