/*
Copyright (C) 2015 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <iostream>
#include <memory>
#include <queue>
#include <random>
#include <set>
#include <sstream>
#include "../utils/Cell2d.hpp"
#include "LangtonAnt.hpp"

using namespace ant;

LangtonAnt::LangtonAnt(int width, int height, bool random_cells, state_t number_of_states)
        : CellularAutomaton2d(number_of_states, width, height)
{
    ant_x_ = (width >> 1);
    ant_y_ = (height >> 1);
    orientation_ = EAntOrientation::Left;
    if (random_cells) {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<unsigned> hd(0, height - 1);
        std::uniform_int_distribution<unsigned> wd(0, width - 1);
        std::normal_distribution<> d((width * height) / 2, (width * height) / 50);
        int num_of_cells = static_cast<int>(d(gen));
        for (int i = 0; i < num_of_cells; ++i) {
            int x = wd(gen);
            int y = hd(gen);
            states_[y][x] = static_cast<state_t>(EFieldState::Active);
        }
    }
}

EAntOrientation LangtonAnt::Turn(EAntOrientation orientation, int direction)
{
    EAntOrientation change_array[4][2] =
            {
                    {EAntOrientation::Down, EAntOrientation::Up},
                    {EAntOrientation::Left, EAntOrientation::Right},
                    {EAntOrientation::Up, EAntOrientation::Down},
                    {EAntOrientation::Right, EAntOrientation::Left}
            };
    return change_array[static_cast<unsigned>(orientation_)][direction];
}

void LangtonAnt::MoveAnt()
{
    if (states_[ant_y_][ant_x_] == static_cast<state_t>(EFieldState::Active)) {
        orientation_ = Turn(orientation_, 1);
    } else {
        orientation_ = Turn(orientation_, 0);
    }
    switch(orientation_)
    {
        case EAntOrientation::Left: {--ant_x_; break;}
        case EAntOrientation::Right: {++ant_x_; break;}
        case EAntOrientation::Up: {--ant_y_; break;}
        case EAntOrientation::Down: {++ant_y_; break;}
    }
    ant_x_ += width_;
    ant_x_ %= width_;
    ant_y_ += height_;
    ant_y_ %= height_;
}

std::vector<std::unique_ptr<Atom>> LangtonAnt::Step(int)
{
    std::vector<std::unique_ptr<Atom>> cells;
    states_[ant_y_][ant_x_] = (states_[ant_y_][ant_x_] == static_cast<state_t>(EFieldState::Active)) ?
                              static_cast<state_t>(EFieldState::InActive) :
                              static_cast<state_t>(EFieldState::Active);
    cells.push_back(std::make_unique<Cell2d>(ant_x_, ant_y_, static_cast<state_t>(states_[ant_y_][ant_x_])));
    MoveAnt();
    return cells;
}

std::string LangtonAnt::Status() const
{
    std::ostringstream ostr;
    ostr << "Ant position: (" << ant_x_ << ", " << ant_y_ << '\n';
    for (const auto& v : states_) {
        for (const auto& state : v) {
            ostr << state;
        }
        ostr << '\n';
    }
    return ostr.str();
}
