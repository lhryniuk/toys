/*
Copyright (C) 2015 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <cmath>
#include <memory>
#include <random>
#include <queue>
#include <set>
#include <sstream>
#include "../utils/Cell2d.hpp"
#include "ElementaryAutomata.hpp"


ElementaryAutomata::ElementaryAutomata(int width, int height, bool random_cells, int rule, state_t number_of_states)
        : CellularAutomaton2d(number_of_states, width, height), rule_{rule}, active_row_{1}
{
    if (random_cells) {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<unsigned> hd(0, height - 1);
        std::uniform_int_distribution<unsigned> wd(0, width - 1);
        std::normal_distribution<> d((width * height) / 2, (width * height) / 50);
        int num_of_cells = static_cast<int>(d(gen));
        for (int i = 0; i < num_of_cells; ++i) {
            int x = wd(gen);
            int y = hd(gen);
            states_[y][x] = 1;
        }
    }
    for (int i = 0; i < width_; ++i) {
        states_[0][i] = 0;
    }
    states_[0][width_ >> 1] = 1;
}


std::vector<std::unique_ptr<Atom>> ElementaryAutomata::Step(int)
{
    std::vector<std::unique_ptr<Atom>> cells;
    if (active_row_ < height_) {
        auto neighbourhood = [this](int i) -> int
        {
            int n = 0;
            for (auto j = -1; j <= 1; ++j) {
                if (0 <= i + j && i + j < width_) {
                    n += states_[active_row_-1][i+j]
                    * static_cast<int>(pow(2.0, static_cast<double>(1-j)));
                }
            } 
            return n;
        };
        auto new_state = [this](int neighbourhood)
        {
            return ((rule_ & (1 << neighbourhood)) ? 1 : 0);
        };
        for (int i = 0; i < width_; ++i) {
            states_[active_row_][i] = new_state(neighbourhood(i));
            cells.push_back(std::make_unique<Cell2d>(i, active_row_, states_[active_row_][i]));
        }
        ++active_row_;
    }
    return cells;
}

std::string ElementaryAutomata::Status() const
{
    std::ostringstream ostr;
    for (const auto& v : states_) {
        for (const auto& state : v) {
            ostr << state;
        }
        ostr << '\n';
    }
    return ostr.str();
}
