/*
Copyright (C) 2015 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <cassert>
#include <cmath>
#include <sstream>

#include "PeterDeJongAttractor.hpp"

PeterDeJongAttractor::PeterDeJongAttractor(long double x, long double y,  std::vector<long double> coef)
    : Attractor2d(x, y), coef_(coef)
{
    assert(coef.size() <= 4);
}

std::unique_ptr<Atom> PeterDeJongAttractor::GenerateNextPoint()
{
    long double new_x = sin(coef_[0] * y_) - cos(coef_[1] * x_);
    long double new_y = sin(coef_[2] * x_) - cos(coef_[3] * y_);
    std::swap(x_, new_x);
    std::swap(y_, new_y);
    return std::move(std::make_unique<Point2d>(x_, y_, 1));
}

void PeterDeJongAttractor::Reset()
{
    x_ = 1.0;
    y_ = 1.0;
}

std::vector<std::unique_ptr<Atom>> PeterDeJongAttractor::Step(int nbr_of_loops)
{
    std::vector<std::unique_ptr<Atom>> atoms;
    for (int i = 0; i < nbr_of_loops; ++i) {
        atoms.push_back(GenerateNextPoint());
    }
    return atoms;
}

std::string PeterDeJongAttractor::Status() const
{
    std::stringstream ss;
    ss << "Current point = (" << x_ << ", " << y_ << ")\n";
    return  ss.str();
}
