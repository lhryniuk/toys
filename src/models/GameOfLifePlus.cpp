/*
Copyright (C) 2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cassert>
#include <iostream>
#include <random>
#include <sstream>

#include "Cell2d.hpp"
#include "GameOfLifePlus.hpp"

GameOfLifePlus::GameOfLifePlus(int width, int height, bool random_cells, unsigned number_of_states)
    : GameOfLife(width, height, random_cells, number_of_states), state_change_(512, 0)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<unsigned> sd(0, number_of_states - 1);
    for (auto&& s : state_change_) {
        s = sd(gen);
    }
}

unsigned GameOfLifePlus::NeighbourhoodFunction(unsigned x, unsigned y)
{
    assert(x < static_cast<unsigned>(width_) && y < static_cast<unsigned>(height_));
    int result = 0;
    int bit = 1 << 8;
    for (const auto& n : neighbourhood_full_) {
        int nx = int(x) + n.first;
        int ny = int(y) + n.second;
        auto check = [](int a, int l){return (0 <= a && a < l);};
        if (check(ny, height_) && check(nx, width_) && states_[ny][nx]) {
            result = (result | bit);
        }
        bit = (bit >> 1);
    }
    result = (result | states_[y][x]);
    return result;
}

state_t GameOfLifePlus::GetNewStateOfCell(unsigned x, unsigned y)
{
    assert(x < static_cast<unsigned>(width_) && y < static_cast<unsigned>(height_));
    return state_change_[NeighbourhoodFunction(x, y)];
}
