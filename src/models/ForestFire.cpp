/*
Copyright (C) 2015 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <memory>
#include <queue>
#include <random>
#include <set>
#include <sstream>
#include "../utils/Cell2d.hpp"
#include "ForestFire.hpp"


ForestFire::ForestFire(int width, int height, bool random_cells, state_t number_of_states)
        : CellularAutomaton2d(number_of_states, width, height)
{
    if (random_cells) {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<unsigned> hd(0, height - 1);
        std::uniform_int_distribution<unsigned> wd(0, width - 1);
        std::normal_distribution<> d((width * height) / 2, (width * height) / 50);
        int num_of_cells = static_cast<int>(d(gen));
        for (int i = 0; i < num_of_cells; ++i) {
            int x = wd(gen);
            int y = hd(gen);
            states_[y][x] = static_cast<state_t>(EFieldState::Tree);
        }
    }
}

void ForestFire::SpreadFire(std::vector<std::unique_ptr<Atom>>& cells,
                            std::vector<std::vector<bool>>& visited,
                            int start_x,
                            int start_y)
{
    visited[start_y][start_x] = true;
    cells.push_back(std::make_unique<Cell2d>(start_x, start_y, static_cast<state_t>(EFieldState::Ground)));
    for (const auto& n : neighbourhood_cross_) {
        auto x = start_x  + n.first;
        auto y = start_y + n.second;
        if (((0 <= x && x < width_)
            && (0 <= y && y < height_)
            && (!visited[y][x]))
            && (states_[y][x] == static_cast<state_t>(EFieldState::Tree))) {
            cells.push_back(std::make_unique<Cell2d>(x, y, static_cast<state_t>(EFieldState::Fire)));
            visited[y][x] = true;
        }
    }

}

std::vector<std::unique_ptr<Atom>> ForestFire::Step(int)
{
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<int> sd(0, 100000);
    std::vector<std::vector<bool>> visited(height_, std::vector<bool>(width_, false));
    std::vector<std::unique_ptr<Atom>> cells;
    for (auto y = 0u; y < unsigned(height_); ++y) {
        for (auto x = 0u; x < unsigned(width_); ++x) {
            if (!visited[y][x]) {
                auto old_state = states_[y][x];
                switch (old_state)
                {
                    case static_cast<state_t>(EFieldState::Fire):
                    {
                        SpreadFire(cells, visited, x, y);
                        break;
                    }
                    case static_cast<state_t>(EFieldState::Tree):
                    {
                        if (sd(gen) < fire_chance) {
                            cells.push_back(std::make_unique<Cell2d>(x, y, static_cast<state_t>(EFieldState::Fire)));
                        }
                        break;
                    }
                    case static_cast<state_t>(EFieldState::Ground):
                    {
                        if (sd(gen) < tree_chance) {
                            cells.push_back(std::make_unique<Cell2d>(x, y, static_cast<state_t>(EFieldState::Tree)));
                        }
                    }
                }
            }
        }
    }
    std::for_each(std::begin(cells), std::end(cells),
                  [this](const auto& cell)
                  {
                      states_[cell->GetCoordinates()[1]][cell->GetCoordinates()[0]] = cell->GetState();
                  });
    return cells;
}

std::string ForestFire::Status() const
{
    std::ostringstream ostr;
    for (const auto& v : states_) {
        for (const auto& state : v) {
            ostr << state;
        }
        ostr << '\n';
    }
    return ostr.str();
}
