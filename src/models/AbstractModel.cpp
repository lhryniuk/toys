/*
Copyright (C) 2015 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "AbstractModel.hpp"


std::vector<std::unique_ptr<Atom>> AbstractModel::GetInitialState() const
{
    return std::vector<std::unique_ptr<Atom>>{};
}

std::vector<std::unique_ptr<Atom>> AbstractModel::GetAllAtoms() const
{
    return GetInitialState();
}

std::string AbstractModel::Status() const
{
    return std::string{};
}

int AbstractModel::GetNumberOfAtomsWithState(state_t t) const
{
    return 0;
}

state_t AbstractModel::GetStateAt(int x, int y)
{
    return 0;
}

void AbstractModel::SetStateAt(int x, int y, state_t new_state)
{
    return;
}
