/*
Copyright (C) 2015 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "CellularAutomaton2d.hpp"
#include "../utils/Cell2d.hpp"


CellularAutomaton2d::CellularAutomaton2d(state_t number_of_states,
                                                   int width,
                                                   int height)
    : CellularAutomaton(number_of_states),
      width_{width},
      height_{height},
      states_{std::vector<std::vector<state_t>>(height, std::vector<state_t>(width, 0))}
{ }

void CellularAutomaton2d::Reset()
{
    states_.clear();
    states_ = std::vector<std::vector<state_t>>(height_, std::vector<state_t>(width_, 0));
}

std::vector<std::unique_ptr<Atom>> CellularAutomaton2d::GetInitialState() const
{
    std::vector<std::unique_ptr<Atom>> atoms;
    for (auto y = 0u; y < unsigned(height_); ++y) {
        for (auto x = 0u; x < unsigned(width_); ++x) {
            if (states_[y][x] != 0) {
                atoms.push_back(std::make_unique<Cell2d>(x, y, states_[y][x]));
            }
        }
    }
    return atoms;
}

int CellularAutomaton2d::GetNumberOfAtomsWithState(state_t state) const
{
    int nbr_of_atoms{};
    for (const auto& row : states_) {
        for (const auto& s : row) {
            if (s == state) {
                ++nbr_of_atoms;
            }
        }
    }
    return nbr_of_atoms;
}

state_t CellularAutomaton2d::GetStateAt(int x, int y)
{
    return states_[y][x];
}

void CellularAutomaton2d::SetStateAt(int x, int y, state_t new_state)
{
    states_[y][x] = new_state;
}

std::vector<std::unique_ptr<Atom>> CellularAutomaton2d::Step(int)
{
    return std::vector<std::unique_ptr<Atom>>{};
}

std::vector<std::unique_ptr<Atom>> CellularAutomaton2d::GetAllAtoms() const {
    std::vector<std::unique_ptr<Atom>> atoms;
    for (auto y = 0u; y < unsigned(height_); ++y) {
        for (auto x = 0u; x < unsigned(width_); ++x) {
            atoms.push_back(std::make_unique<Cell2d>(x, y, states_[y][x]));
        }
    }
    return atoms;
}
