/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <random>

#include "SDLController.hpp"
#include "SDLView.hpp"

#include "ModelFactory.hpp"

const auto cell_size = 4;
const auto width = 1200;
const auto height = 880;
const auto delay = 100;
const auto automata_width = width / cell_size;
const auto automata_height = height / cell_size;

ModelFactory model_factory(width / cell_size, height / cell_size);

void run(std::shared_ptr<std::unique_ptr<AbstractModel>> model, TColorVector colors, int cell_size, int width, int height, int loops_at_once)
{
    SDLView gol(model, colors, cell_size, width, height, loops_at_once);
    gol.MainLoop(delay);
}

void elementary_automata(int rule)
{
    auto model = model_factory.get_class<ElementaryAutomata>(automata_width, automata_height, false, rule);
    run(std::make_shared<std::unique_ptr<AbstractModel>>(std::move(model)), colors::black_white, cell_size, width, height, 1);
}

void life_plus()
{
    auto model = model_factory.get_class<GameOfLifePlus>(automata_width, automata_height);
    run(std::make_shared<std::unique_ptr<AbstractModel>>(std::move(model)), colors::black_white, cell_size, width, height, 1);
}


int main()
{
    life_plus();
    return 0;
}
