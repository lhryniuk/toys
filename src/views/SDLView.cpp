/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <cassert>
#include <iostream>
#include <memory>

#include "SDL2/SDL_video.h"

#include "SDLController.hpp"
#include "SDLView.hpp"


SDLView::SDLView(std::shared_ptr<std::unique_ptr<AbstractModel>> model, TColorVector states_colors,
                 unsigned cell_size, unsigned width, unsigned height, int loops_at_once)
    : GraphicalView(std::make_unique<SDLController>(model), model, states_colors),
      cell_size_{cell_size},
      width_{width},
      height_{height},
      loops_at_once_{loops_at_once}
{
    assert(cell_size > 0);
    assert(width > 0);
    assert(height > 0);
    SDL_Init(SDL_INIT_EVERYTHING);
    screen_ = SDL_CreateWindow("toys", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width_, height_, SDL_WINDOW_SHOWN);
    renderer_ = SDL_CreateRenderer(screen_, -1, SDL_RENDERER_ACCELERATED);
    SDL_SetRenderDrawColor(renderer_, states_colors[0].r_, states_colors[0].g_, states_colors[0].b_, states_colors[0].a_);
    SDL_RenderClear(renderer_);
}


SDLView::~SDLView()
{
    SDL_DestroyRenderer(renderer_);
    SDL_DestroyWindow(screen_);
    renderer_ = nullptr;
    screen_ = nullptr;
    SDL_Quit();
}

void SDLView::DrawAtom(std::unique_ptr<Atom> atom) const
{
    assert(atom);
    EAtomType type = atom->GetType();
    state_t state = atom->GetState();
    assert(0 <= state && state < states_colors_.size());
    SDL_Rect r;
    r.w = r.h = cell_size_;
    auto coord = atom->GetCoordinates();
    switch (type) {
        case EAtomType::Cell2d: {
            r.x = coord[0] * cell_size_;
            r.y = coord[1] * cell_size_;
            SDL_SetRenderDrawColor(renderer_,
                    states_colors_[state].r_,
                    states_colors_[state].g_,
                    states_colors_[state].b_,
                    states_colors_[state].a_);
            SDL_RenderFillRect(renderer_, &r);
            break;
        }
        case EAtomType::Point2d: {
            r.x = (coord[0] * (width_ / 5)) + width_ / 2;
            r.y = (coord[1] * (height_ / 5)) + height_ / 2;
            SDL_SetRenderDrawColor(renderer_,
                                   states_colors_[state].r_,
                                   states_colors_[state].g_,
                                   states_colors_[state].b_,
                                   states_colors_[state].a_);
            SDL_RenderFillRect(renderer_, &r);
            break;
        }
        default: break;
    }
}

void SDLView::ClearScreen()
{
    SDL_Rect r;
    r.w = width_;
    r.h = height_;
    r.x = 0;
    r.y = 0;
    SDL_SetRenderDrawColor(renderer_,
                           states_colors_[0].r_,
                           states_colors_[0].g_,
                           states_colors_[0].b_,
                           states_colors_[0].a_);
    SDL_RenderFillRect(renderer_, &r);
}

void SDLView::DrawAtoms(std::vector<std::unique_ptr<Atom>>&& atoms) const
{
    std::for_each(std::begin(atoms), std::end(atoms),
                  [this](std::unique_ptr<Atom>& atom_ptr){DrawAtom(std::move(atom_ptr));});
}

void SDLView::Update()
{
    bool redraw = false;
    running_ = controller_->ProcessNextEvent(redraw, pause_);
    if (!pause_) {
        DrawAtoms((*model_)->Step(loops_at_once_));
    } else if (redraw && !pause_) {
        ClearScreen();
        DrawAtoms((*model_)->GetAllAtoms());
        redraw = false;
    }
}

void SDLView::MainLoop(unsigned delay)
{
    DrawAtoms((*model_)->GetInitialState());
    while (running_) {
        Update();
        SDL_RenderPresent(renderer_);
        SDL_UpdateWindowSurface(screen_);
        SDL_Delay(delay);
    }
}
