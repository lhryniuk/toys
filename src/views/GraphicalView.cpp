/*
Copyright (C) 2015 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "GraphicalView.hpp"


GraphicalView::GraphicalView(std::unique_ptr<AbstractController> controller,
                             std::shared_ptr<std::unique_ptr<AbstractModel>> model,
                             TColorVector states_colors)
    : AbstractView(std::move(controller), model, states_colors)
{ }
