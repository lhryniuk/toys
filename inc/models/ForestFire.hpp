/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _TOYS_FORESTFIRE_H_
#define _TOYS_FORESTFIRE_H_


#include "CellularAutomaton2d.hpp"


enum class EFieldState : state_t
{
    Ground = 0u,
    Fire = 1u,
    Tree = 2u
};


class ForestFire : public CellularAutomaton2d
{
public:
    ForestFire(int width, int height, bool random_cells=true, state_t number_of_states=3);
    virtual ~ForestFire() = default;
    virtual std::vector<std::unique_ptr<Atom>> Step(int nbr_of_loops=1) override;
    virtual std::string Status() const override;

private:
    void SpreadFire(std::vector<std::unique_ptr<Atom>>& cells,
                    std::vector<std::vector<bool>>& visited,
                    int start_x,
                    int start_y);
    /*
    * Probability is divided by 10000 to provide a simple way to keep a fire chance reasonable small
    */
    const int fire_chance = 1;
    const int tree_chance = 200;
};


#endif //_TOYS_FORESTFIRE_H_
