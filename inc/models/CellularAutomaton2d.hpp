/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CELLULAR_AUTOMATA_2D_MODEL_HPP__
#define __CELLULAR_AUTOMATA_2D_MODEL_HPP__

#include <vector>

#include "Atom.hpp"
#include "CellularAutomaton.hpp"

class CellularAutomaton2d : public CellularAutomaton
{
public:
    CellularAutomaton2d(state_t number_of_states, int width, int height);
    virtual ~CellularAutomaton2d() = default;
    virtual void Reset() override;
    virtual int GetNumberOfAtomsWithState(state_t) const override;
    virtual state_t GetStateAt(int x, int y) override;
    virtual void SetStateAt(int x, int y, state_t new_state) override;
    virtual std::vector<std::unique_ptr<Atom>> Step(int nbr_of_loops=1) override;
    virtual std::vector<std::unique_ptr<Atom>> GetInitialState() const override;
    virtual std::vector<std::unique_ptr<Atom>> GetAllAtoms() const override;


protected:
    const std::vector<std::pair<int, int>> neighbourhood_full_ = {{-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}};
    const std::vector<std::pair<int, int>> neighbourhood_cross_ = {{-1, 0}, {0, -1}, {0, 1}, {1, 0}};
    int width_;
    int height_;
    std::vector<std::vector<state_t>> states_;
};


#endif // __CELLULAR_AUTOMATA_2D_MODEL_HPP__
