/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __ABSTRACT_MODEL_HPP__
#define __ABSTRACT_MODEL_HPP__

#include <memory>
#include <string>
#include <vector>

#include "Atom.hpp"

// TODO: move parameters to map
class AbstractModel
{
public:
    virtual ~AbstractModel() = default;
    virtual void Reset() = 0;
    virtual int GetNumberOfAtomsWithState(state_t) const;
    virtual state_t GetStateAt(int x, int y);
    virtual void SetStateAt(int x, int y, state_t new_state);
    virtual std::vector<std::unique_ptr<Atom>> Step(int nbr_of_loops=1) = 0;
    virtual std::vector<std::unique_ptr<Atom>> GetInitialState() const;
    virtual std::vector<std::unique_ptr<Atom>> GetAllAtoms() const;
    virtual std::string Status() const;
};


#endif // __ABSTRACT_MODEL_HPP__
