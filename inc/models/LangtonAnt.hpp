/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _TOYS_LANGTONANT_H_
#define _TOYS_LANGTONANT_H_


#include "CellularAutomaton2d.hpp"

namespace ant
{
    enum class EFieldState : state_t
    {
        InActive = 0u,
        Active = 1u
    };

    enum class EAntOrientation : unsigned
    {
        Left = 0u,
        Up,
        Right,
        Down
    };
}

class LangtonAnt : public CellularAutomaton2d
{
public:
    LangtonAnt(int width, int height, bool random_cells=false, state_t number_of_states=2);
    virtual ~LangtonAnt() = default;
    virtual std::vector<std::unique_ptr<Atom>> Step(int nbr_of_loops=1) override;
    virtual std::string Status() const override;

private:
    // direction == 1 -> move clockwise, direction == 0 -> counter-clockwise
    ant::EAntOrientation Turn(ant::EAntOrientation orientation, int direction);
    void MoveAnt();
    ant::EAntOrientation orientation_;
    int ant_x_;
    int ant_y_;
};


#endif //_TOYS_LANGTONANT_H_
