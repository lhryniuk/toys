/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TOYS_PETERDEJONGATTRACTOR_H
#define TOYS_PETERDEJONGATTRACTOR_H

#include "Attractor2d.hpp"

class PeterDeJongAttractor : public Attractor2d
{
public:
    PeterDeJongAttractor(long double x, long double y, std::vector<long double> coef);
    virtual ~PeterDeJongAttractor() = default;
    virtual std::unique_ptr<Atom> GenerateNextPoint() override;
    virtual void Reset() override;
    virtual std::vector<std::unique_ptr<Atom>> Step(int nbr_of_loops=1) override;
    virtual std::string Status() const override;
private:
    std::vector<long double> coef_;
};


#endif //TOYS_PETERDEJONGATTRACTOR_H
