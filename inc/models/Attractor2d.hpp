/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TOYS_ATTRACTOR2D_H
#define TOYS_ATTRACTOR2D_H

#include "Attractor.hpp"
#include "Point2d.hpp"

class Attractor2d : public Attractor
{
public:
    Attractor2d(long double x, long double y);
    virtual ~Attractor2d() = default;
    virtual std::unique_ptr<Atom> GenerateNextPoint() = 0;

protected:
    long double x_;
    long double y_;
};


#endif //TOYS_ATTRACTOR2D_H
