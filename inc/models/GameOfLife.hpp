/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __GAME_OF_LIFE_HPP__
#define __GAME_OF_LIFE_HPP__

#include <string>
#include <utility>

#include "CellularAutomaton2d.hpp"


class GameOfLife : public CellularAutomaton2d
{
public:
    GameOfLife(int width, int height, bool random_cells=true, unsigned number_of_states=2);
    virtual ~GameOfLife() = default;
    virtual std::string Status() const override;
    virtual std::vector<std::unique_ptr<Atom>> Step(int nbr_of_loops=1) override;

protected:
    int number_of_alive_cells_;

private:
    unsigned CountCellNeighbours(unsigned x, unsigned y);
    virtual state_t GetNewStateOfCell(unsigned x, unsigned y);
    int number_of_cells_;
};


#endif // __GAME_OF_LIFE_HPP__
