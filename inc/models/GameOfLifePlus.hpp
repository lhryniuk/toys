/*
Copyright (C) 2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __GAME_OF_LIFE_PLUS_HPP__
#define __GAME_OF_LIFE_PLUS_HPP__

#include <string>
#include <utility>
#include <vector>

#include "GameOfLife.hpp"


class GameOfLifePlus : public GameOfLife
{
public:
    GameOfLifePlus(int width, int height, bool random_cells=true, unsigned number_of_states=2);
    virtual ~GameOfLifePlus() = default;

private:
    unsigned NeighbourhoodFunction(unsigned x, unsigned y);
    virtual state_t GetNewStateOfCell(unsigned x, unsigned y);
    std::vector<state_t> state_change_;
};


#endif // __GAME_OF_LIFE_PLUS_HPP__
