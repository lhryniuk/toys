/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _TOYS_ELEMENTARYAUTOMATA_H_
#define _TOYS_ELEMENTARYAUTOMATA_H_


#include "CellularAutomaton2d.hpp"


class ElementaryAutomata : public CellularAutomaton2d
{
public:
    ElementaryAutomata(int width, int height, bool random_cells=false, int rule=90, state_t number_of_states=2);
    virtual ~ElementaryAutomata() = default;
    virtual std::vector<std::unique_ptr<Atom>> Step(int nbr_of_loops=1) override;
    virtual std::string Status() const override;

private:
    int rule_;
    int active_row_;
};


#endif //_TOYS_ELEMENTARYAUTOMATA_H_
