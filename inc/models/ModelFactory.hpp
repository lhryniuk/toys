/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TOYS_MODELFACTORY_H
#define TOYS_MODELFACTORY_H

#include <string>

#include "AbstractModel.hpp"
#include "ForestFire.hpp"
#include "PeterDeJongAttractor.hpp"
#include "GameOfLife.hpp"
#include "GameOfLifePlus.hpp"
#include "LangtonAnt.hpp"
#include "ElementaryAutomata.hpp"


class ModelFactory
{
public:
    ModelFactory(int width, int height)
        : width_{width}, height_{height}
    {}

    template<typename T, typename... Args>
    std::unique_ptr<AbstractModel> get_class(Args&&... args)
    {
        return std::make_unique<T>(std::forward<Args>(args)...);
    }

private:
    int width_;
    int height_;
};


#endif //TOYS_MODELFACTORY_H
