/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __ABSTRACT_VIEW_HPP__
#define __ABSTRACT_VIEW_HPP__

#include <memory>

#include "../utils/Color.hpp"
#include "../models/AbstractModel.hpp"
#include "../controllers/AbstractController.hpp"

class AbstractView
{
public:
    AbstractView(std::unique_ptr<AbstractController> controller,
                 std::shared_ptr<std::unique_ptr<AbstractModel>> model,
                 std::vector<Color> states_colors);
    virtual void DrawAtom(std::unique_ptr<Atom> atom) const = 0;
    virtual void DrawAtoms(std::vector<std::unique_ptr<Atom>>&& atoms) const = 0;
    virtual void Update() = 0;
    virtual ~AbstractView() = default;

protected:
    std::unique_ptr<AbstractController> controller_;
    std::shared_ptr<std::unique_ptr<AbstractModel>> model_;
    TColorVector states_colors_;
    bool running_;
    bool pause_;
};


#endif // __ABSTRACT_VIEW_HPP__
