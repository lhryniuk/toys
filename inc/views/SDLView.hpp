/*
Copyright (C) 2015 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __SDL_VIEW_HPP__
#define __SDL_VIEW_HPP__

#include <memory>

#include "SDL.h"
//#include "SDL_ttf.h"

#include "GraphicalView.hpp"
#include "../utils/Atom.hpp"

class SDLView : public GraphicalView
{
public:
    SDLView(std::shared_ptr<std::unique_ptr<AbstractModel>> model, TColorVector states_colors,
            unsigned cell_size=2, unsigned width=500, unsigned height=500, int loops_at_once=1);
    virtual ~SDLView();
    void MainLoop(unsigned delay);
    void ClearScreen();
    void DrawAtom(std::unique_ptr<Atom> atom) const override;
    virtual void DrawAtoms(std::vector<std::unique_ptr<Atom>>&& atoms) const override;
    virtual void Update() override;

private:
    SDL_Window *screen_;
    SDL_Renderer *renderer_;
    unsigned cell_size_;
    unsigned width_;
    unsigned height_;
    int loops_at_once_;
};


#endif // __SDL_VIEW_HPP__
