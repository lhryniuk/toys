/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __ATOM_HPP__
#define __ATOM_HPP__

#include <vector>

#include "Color.hpp"

typedef unsigned state_t;

enum class EAtomType : unsigned char
{
    None = 0,
    Point2d,
    Point3d,
    Cell2d,
    Cell3d
};


class Atom
{
public:
    Atom(EAtomType type=EAtomType::None, state_t state=0, Color color=colors::black);
    virtual ~Atom() = default;
    virtual std::vector<long double> GetCoordinates() const = 0;
    virtual EAtomType GetType() const final;
    virtual state_t GetState() const final;

private:
    EAtomType type_;
    state_t state_;
    Color color_;
};


#endif // __ATOM_HPP__
