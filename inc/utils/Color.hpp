/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef _TOYS_COLOR_HPP_
#define _TOYS_COLOR_HPP_

#include <cstdint>
#include <vector>

struct Color
{
    Color(uint8_t r, uint8_t g, uint8_t b, uint8_t a=uint8_t(255))
        : r_{r}, g_{g}, b_{b}, a_{a} { }
    void Shade()
    {
        a_ = std::max(0, a_ - 1);
    }
    uint8_t r_;
    uint8_t g_;
    uint8_t b_;
    uint8_t a_;
};

using TColorVector = std::vector<Color>;

namespace colors
{

extern const Color black;
extern const Color white;
extern const Color ground_brown;
extern const Color fire;
extern const Color tree_green;

extern const TColorVector black_white;
extern const TColorVector forest_fire;

}

#endif //_TOYS_COLOR_HPP_
