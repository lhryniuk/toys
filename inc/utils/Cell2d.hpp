/*
Copyright (C) 2015 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef __CELL_2D_HPP__
#define __CELL_2D_HPP__

#include "Cell.hpp"


class Cell2d : public Cell
{
public:
    Cell2d(long double x=0, long double y=0, unsigned state=0);
    virtual ~Cell2d() = default;
    virtual std::vector<long double> GetCoordinates() const override;

private:
    long double x_;
    long double y_;
};


#endif // __CELL_2D_HPP__
