/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>

#include <cppunit/TestCase.h>
#include <cppunit/TestSuite.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestResult.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/ui/text/TextTestRunner.h>

#include "AbstractModel.hpp"
#include "ElementaryAutomata.hpp"


class TElementaryAutomata : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TElementaryAutomata);
    CPPUNIT_TEST(testNinety);
    CPPUNIT_TEST(testSeventyTwo);
    CPPUNIT_TEST_SUITE_END();

public:
    TElementaryAutomata()
            : CppUnit::TestCase(), width_eight_(8), width_five_(5), height_(5),
              model_ninety_{std::make_unique<ElementaryAutomata>(width_five_, height_, false)},
              model_seventy_two_{std::make_unique<ElementaryAutomata>(width_eight_, height_, false, 72)}
    {}
    void testNinety()
    {
        /*
         * Rule 90:
         * 90 = 01011010
         * 111, 110, 101, 100, 011, 010, 001, 000
         *  0    1    0    1    1    0    1    0
         * */
        model_ninety_->Step();
        /* 0 1 0 1 0 */
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_ninety_->GetStateAt(0, 1));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_ninety_->GetStateAt(1, 1));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_ninety_->GetStateAt(2, 1));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_ninety_->GetStateAt(3, 1));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_ninety_->GetStateAt(4, 1));
        model_ninety_->Step();
        /* 1 0 0 0 1 */
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_ninety_->GetStateAt(0, 2));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_ninety_->GetStateAt(1, 2));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_ninety_->GetStateAt(2, 2));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_ninety_->GetStateAt(3, 2));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_ninety_->GetStateAt(4, 2));
        model_ninety_->Step();
        /* 0 1 0 1 0 */
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_ninety_->GetStateAt(0, 3));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_ninety_->GetStateAt(1, 3));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_ninety_->GetStateAt(2, 3));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_ninety_->GetStateAt(3, 3));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_ninety_->GetStateAt(4, 3));
        model_ninety_->Step();
        /* 1 0 0 0 1 */
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_ninety_->GetStateAt(0, 4));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_ninety_->GetStateAt(1, 4));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_ninety_->GetStateAt(2, 4));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_ninety_->GetStateAt(3, 4));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_ninety_->GetStateAt(4, 4));
    }
    void testSeventyTwo()
    {
        /*
         * Rule 72:
         * 72 = 01001000
         * 111, 110, 101, 100, 011, 010, 001, 000
         *  0    1    0    0    1    0    0    0
         * */
        model_seventy_two_->SetStateAt(0, 0, 1);
        model_seventy_two_->SetStateAt(1, 0, 1);
        model_seventy_two_->SetStateAt(2, 0, 1);
        model_seventy_two_->SetStateAt(3, 0, 0);
        model_seventy_two_->SetStateAt(4, 0, 1);
        model_seventy_two_->SetStateAt(5, 0, 1);
        model_seventy_two_->SetStateAt(6, 0, 0);
        model_seventy_two_->SetStateAt(7, 0, 1);
        /* 1 1 1 0 1 1 0 1 */
        model_seventy_two_->Step();
        /* 1 0 1 0 1 1 0 0 */
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_seventy_two_->GetStateAt(0, 1));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(1, 1));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_seventy_two_->GetStateAt(2, 1));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(3, 1));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_seventy_two_->GetStateAt(4, 1));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_seventy_two_->GetStateAt(5, 1));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(6, 1));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(7, 1));
        model_seventy_two_->Step();
        /* 0 0 0 0 1 1 0 0 */
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(0, 2));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(1, 2));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(2, 2));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(3, 2));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_seventy_two_->GetStateAt(4, 2));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_seventy_two_->GetStateAt(5, 2));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(6, 2));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(7, 2));
        model_seventy_two_->Step();
        /* 0 0 0 0 1 1 0 0 */
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(0, 3));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(1, 3));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(2, 3));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(3, 3));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_seventy_two_->GetStateAt(4, 3));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(1), model_seventy_two_->GetStateAt(5, 3));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(6, 3));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(0), model_seventy_two_->GetStateAt(7, 3));
    }

private:
    const int width_eight_;
    const int width_five_;
    const int height_;
    std::unique_ptr<AbstractModel> model_ninety_;
    std::unique_ptr<AbstractModel> model_seventy_two_;
};

CPPUNIT_TEST_SUITE_REGISTRATION(TElementaryAutomata);
