/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>

#include <cppunit/TestCase.h>
#include <cppunit/TestSuite.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestResult.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/ui/text/TextTestRunner.h>

#include "AbstractModel.hpp"
#include "GameOfLife.hpp"


class TGameOfLife : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TGameOfLife);
    CPPUNIT_TEST(testEmpty);
    CPPUNIT_TEST(testOneCellDeath);
    CPPUNIT_TEST(testBlinkerStep);
    CPPUNIT_TEST_SUITE_END();

public:
    TGameOfLife()
            : CppUnit::TestCase(), width_(5), height_(5),
              model_empty_ {std::make_unique<GameOfLife>(width_, height_, false)},
              model_one_cell_{std::make_unique<GameOfLife>(width_, height_, false)},
              model_blinker_{std::make_unique<GameOfLife>(width_, height_, false)}
    {
        model_one_cell_->SetStateAt(2, 2, 1);
        model_blinker_->SetStateAt(1, 1, 1);
        model_blinker_->SetStateAt(2, 1, 1);
        model_blinker_->SetStateAt(3, 1, 1);
    };
    void testEmpty()
    {
        CPPUNIT_ASSERT_EQUAL(width_ * height_, model_empty_->GetNumberOfAtomsWithState(0));
    }
    void testOneCellDeath()
    {
        CPPUNIT_ASSERT_EQUAL(1, model_one_cell_->GetNumberOfAtomsWithState(1));
        model_one_cell_->Step();
        CPPUNIT_ASSERT_EQUAL(0, model_one_cell_->GetNumberOfAtomsWithState(1));
    }
    void testBlinkerStep()
    {
        CPPUNIT_ASSERT_EQUAL(3, model_blinker_->GetNumberOfAtomsWithState(1));
        model_blinker_->Step();
        CPPUNIT_ASSERT_EQUAL(3, model_blinker_->GetNumberOfAtomsWithState(1));
        CPPUNIT_ASSERT_EQUAL(1u, model_blinker_->GetStateAt(2, 0));
        CPPUNIT_ASSERT_EQUAL(1u, model_blinker_->GetStateAt(2, 1));
        CPPUNIT_ASSERT_EQUAL(1u, model_blinker_->GetStateAt(2, 2));
    }
private:
    const int width_;
    const int height_;
    std::unique_ptr<AbstractModel> model_empty_;
    std::unique_ptr<AbstractModel> model_one_cell_;
    std::unique_ptr<AbstractModel> model_blinker_;
};

CPPUNIT_TEST_SUITE_REGISTRATION(TGameOfLife);
