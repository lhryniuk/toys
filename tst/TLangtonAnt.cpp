/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>

#include <cppunit/TestCase.h>
#include <cppunit/TestSuite.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestResult.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/ui/text/TextTestRunner.h>

#include "AbstractModel.hpp"
#include "LangtonAnt.hpp"

using namespace ant;


class TLangtonAnt : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TLangtonAnt);
    CPPUNIT_TEST(testOneStep);
    CPPUNIT_TEST_SUITE_END();

public:
    TLangtonAnt()
            : CppUnit::TestCase(), width_(5), height_(5),
              model_one_step_{std::make_unique<LangtonAnt>(width_, height_, false)}
    {}
    void testOneStep()
    {
        model_one_step_->Step();
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(EFieldState::Active), model_one_step_->GetStateAt(2, 2));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(EFieldState::InActive), model_one_step_->GetStateAt(2, 1));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(EFieldState::InActive), model_one_step_->GetStateAt(1, 2));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(EFieldState::InActive), model_one_step_->GetStateAt(3, 2));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(EFieldState::InActive), model_one_step_->GetStateAt(2, 3));
    }

private:
    const int width_;
    const int height_;
    std::unique_ptr<AbstractModel> model_one_step_;
};

CPPUNIT_TEST_SUITE_REGISTRATION(TLangtonAnt);
