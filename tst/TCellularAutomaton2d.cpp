/*
Copyright (C) 2015 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <algorithm>
#include <memory>
#include <numeric>

#include <cppunit/TestCase.h>
#include <cppunit/TestSuite.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestResult.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/ui/text/TextTestRunner.h>

#include "AbstractModel.hpp"
#include "CellularAutomaton2d.hpp"


class TCellularAutomaton2d : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TCellularAutomaton2d);
    CPPUNIT_TEST(testReset);
    CPPUNIT_TEST_SUITE_END();

public:
    TCellularAutomaton2d()
            : CppUnit::TestCase(), width_(5), height_(5),
              model_{std::make_unique<CellularAutomaton2d>(4, width_, height_)}
    {
        model_->SetStateAt(1, 1, static_cast<state_t>(1));
        model_->SetStateAt(2, 3, static_cast<state_t>(1));
        model_->SetStateAt(4, 1, static_cast<state_t>(1));
        model_->SetStateAt(4, 4, static_cast<state_t>(1));
        model_->SetStateAt(4, 2, static_cast<state_t>(2));
        model_->SetStateAt(1, 2, static_cast<state_t>(3));
        model_->SetStateAt(1, 4, static_cast<state_t>(3));
        model_->SetStateAt(3, 3, static_cast<state_t>(3));
    }
    void testReset()
    {
        const std::vector<int> nbr_of_states{0, 4, 1, 3};
        auto sum = std::accumulate(std::begin(nbr_of_states), std::end(nbr_of_states), 0);
        CPPUNIT_ASSERT_EQUAL(nbr_of_states[1], model_->GetNumberOfAtomsWithState(1));
        CPPUNIT_ASSERT_EQUAL(nbr_of_states[2], model_->GetNumberOfAtomsWithState(2));
        CPPUNIT_ASSERT_EQUAL(nbr_of_states[3], model_->GetNumberOfAtomsWithState(3));
        CPPUNIT_ASSERT_EQUAL(width_ * height_ - sum, model_->GetNumberOfAtomsWithState(0));
        model_->Reset();
        CPPUNIT_ASSERT_EQUAL(0, model_->GetNumberOfAtomsWithState(1));
        CPPUNIT_ASSERT_EQUAL(0, model_->GetNumberOfAtomsWithState(2));
        CPPUNIT_ASSERT_EQUAL(0, model_->GetNumberOfAtomsWithState(3));
    }

private:
    const int width_;
    const int height_;
    std::unique_ptr<AbstractModel> model_;
};

CPPUNIT_TEST_SUITE_REGISTRATION(TCellularAutomaton2d);
