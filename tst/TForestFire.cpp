/*
Copyright (C) 2015,2016 Lukasz Hryniuk

This file is part of toys.

toys is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

toys is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with toys.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>

#include <cppunit/TestCase.h>
#include <cppunit/TestSuite.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/TestResult.h>
#include <cppunit/CompilerOutputter.h>
#include <cppunit/TestRunner.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/ui/text/TextTestRunner.h>

#include "AbstractModel.hpp"
#include "ForestFire.hpp"


class TForestFire : public CppUnit::TestCase
{
    CPPUNIT_TEST_SUITE(TForestFire);
    CPPUNIT_TEST(testSpread);
    CPPUNIT_TEST(testFireStop);
    CPPUNIT_TEST_SUITE_END();

public:
    TForestFire()
            : CppUnit::TestCase(), width_(5), height_(5),
              model_spread_{std::make_unique<ForestFire>(width_, height_, false)},
              model_fire_stop_{std::make_unique<ForestFire>(width_, height_, false)}
    {
        model_spread_->SetStateAt(1, 1, static_cast<state_t>(EFieldState::Tree));
        model_spread_->SetStateAt(2, 1, static_cast<state_t>(EFieldState::Fire));
        model_spread_->SetStateAt(3, 1, static_cast<state_t>(EFieldState::Tree));

        model_fire_stop_->SetStateAt(1, 1, static_cast<state_t>(EFieldState::Fire));
    }
    void testSpread()
    {
        model_spread_->Step();
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(EFieldState::Fire), model_spread_->GetStateAt(1, 1));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(EFieldState::Ground), model_spread_->GetStateAt(2, 1));
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(EFieldState::Fire), model_spread_->GetStateAt(3, 1));
    }
    void testFireStop()
    {
        model_fire_stop_->Step();
        CPPUNIT_ASSERT_EQUAL(static_cast<state_t>(EFieldState::Ground), model_fire_stop_->GetStateAt(1, 1));
    }

private:
    const int width_;
    const int height_;
    std::unique_ptr<AbstractModel> model_spread_;
    std::unique_ptr<AbstractModel> model_fire_stop_;
};

CPPUNIT_TEST_SUITE_REGISTRATION(TForestFire);
